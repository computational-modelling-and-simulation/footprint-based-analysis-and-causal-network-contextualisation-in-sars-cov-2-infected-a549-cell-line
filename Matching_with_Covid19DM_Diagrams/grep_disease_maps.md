### License Info

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

Please check <http://www.gnu.org/licenses/>.

The code included in the present notebook is based on this
[script](https://git-r3lab.uni.lu/computational-modelling-and-simulation/generegulationanalysis/-/blob/master/5_SBMLSearch/SourceCode/grep_disease_map.R)
developed by Yusuke Hiki

Introduction
============

The present script takes the output of our Footprint-based analysis and
matches the results againts the content of [the Covid-19 Disease
Maps](https://covid.pages.uni.lu/map_contents)

    library(kableExtra)
    library(stringr)

Results
=======

Reading Input files
-------------------

We first read the content ofthe Covid-19 Disease Maps which can be found
in this
[file](https://gitlab.lcsb.uni.lu/covid/models/-/blob/master/Resources/Expand%20the%20diagrams/COVID19_Disease_Map_bipartite_crosslinked.tsv):

    disease_map <- read.delim("https://gitlab.lcsb.uni.lu/covid/models/-/raw/master/Resources/Expand%20the%20diagrams/COVID19_Disease_Map_bipartite_crosslinked.tsv")

We then read the output of our Footprint-based analyisis, namely
CARNIVAL's output network. This file can be found
[here](https://gitlab.lcsb.uni.lu/computational-modelling-and-simulation/footprint-based-analysis-and-causal-network-contextualisation-in-sars-cov-2-infected-a549-cell-line/-/tree/master/Carnival_Results).

    carnival_results <- readRDS("InputFiles/carnival_results_withprogeny.rds")
    carnival_nodes_hgnc <- unique(c(carnival_results$weightedSIF[,"Node1"], carnival_results$weightedSIF[,"Node2"]))

Matching the results
--------------------

We finally match all our genes from the carnival network with all the
genes from the different pathways included in the COVID19 Disease maps.

    matching_genes <- character()
    matching_pathways <- character()

    for (i in 1:nrow(disease_map)){
      
      if (!(is.na(disease_map$source_hgnc[i]))) {
        
        source_nodes <- 
          str_replace(unlist(strsplit(disease_map$source_hgnc[i], split = ";")), "HGNC_SYMBOL:", "")
        
      } else {
        source_nodes <- c()
      }
      
      if (!(is.na(disease_map$target_hgnc[i]))) {
        
        target_nodes <- 
          str_replace(unlist(strsplit(disease_map$target_hgnc[i], split = ";")), "HGNC_SYMBOL:", "")
        
      }  else {
        target_nodes <- c()
      }
      
      all_nodes <- unique(c(source_nodes, target_nodes))
      
      if (length(all_nodes)!= 0){
        current_matching_genes <- intersect(carnival_nodes_hgnc, all_nodes)
        
        if (length(current_matching_genes) != 0){
          
          current_matching_pathways <-  rep(disease_map$source_diagram[i], length(current_matching_genes))
          matching_genes <- c(matching_genes, current_matching_genes)
          matching_pathways <- c(matching_pathways, current_matching_pathways)
        }
        
      }
      
    }

    disease_map_hgnc_carnival_detected <- 
      data.frame(source_diagram = matching_pathways, hgnc_carnival=matching_genes) %>% 
      dplyr::distinct()

and We visualize the results.

    disease_map_hgnc_carnival_detected %>% 
      kbl(col.names = c("Covid19 DM Diagram", "Carnival nodes")) %>% 
      kable_styling()

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Covid19 DM Diagram
</th>
<th style="text-align:left;">
Carnival nodes
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
C19DMap:Interferon 1 pathway
</td>
<td style="text-align:left;">
IRF3
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Interferon 1 pathway
</td>
<td style="text-align:left;">
TBK1
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Interferon 1 pathway
</td>
<td style="text-align:left;">
IKBKE
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:TGFbeta signalling
</td>
<td style="text-align:left;">
MAPK3
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:TGFbeta signalling
</td>
<td style="text-align:left;">
SMAD1
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:PAMP signalling
</td>
<td style="text-align:left;">
TICAM1
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:PAMP signalling
</td>
<td style="text-align:left;">
IKBKE
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:PAMP signalling
</td>
<td style="text-align:left;">
IRF3
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:PAMP signalling
</td>
<td style="text-align:left;">
TBK1
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Pyrimidine deprivation
</td>
<td style="text-align:left;">
IRF3
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Pyrimidine deprivation
</td>
<td style="text-align:left;">
TBK1
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Orf3a protein interactions
</td>
<td style="text-align:left;">
TICAM1
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Endoplasmatic Reticulum stress
</td>
<td style="text-align:left;">
ATF6
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Endoplasmatic Reticulum stress
</td>
<td style="text-align:left;">
ATF4
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Endoplasmatic Reticulum stress
</td>
<td style="text-align:left;">
MBTPS1
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Interferon lambda pathway
</td>
<td style="text-align:left;">
IRF3
</td>
</tr>
<tr>
<td style="text-align:left;">
C19DMap:Interferon lambda pathway
</td>
<td style="text-align:left;">
TBK1
</td>
</tr>
</tbody>
</table>
    write.table(x=disease_map_hgnc_carnival_detected, 
                file = "MatchingGenes/carnival_diagrams.tsv", sep = "\t",
                row.names = FALSE, quote = FALSE)

Session Info Details
====================

    ## R version 4.0.4 (2021-02-15)
    ## Platform: x86_64-w64-mingw32/x64 (64-bit)
    ## Running under: Windows 10 x64 (build 19042)
    ## 
    ## Matrix products: default
    ## 
    ## locale:
    ## [1] LC_COLLATE=English_Germany.1252  LC_CTYPE=English_Germany.1252   
    ## [3] LC_MONETARY=English_Germany.1252 LC_NUMERIC=C                    
    ## [5] LC_TIME=English_Germany.1252    
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ## [1] stringr_1.4.0    kableExtra_1.3.4
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] highr_0.9         compiler_4.0.4    pillar_1.7.0      tools_4.0.4      
    ##  [5] digest_0.6.27     evaluate_0.14     lifecycle_1.0.1   tibble_3.1.6     
    ##  [9] viridisLite_0.4.0 pkgconfig_2.0.3   rlang_1.0.1       DBI_1.1.2        
    ## [13] cli_3.2.0         rstudioapi_0.13   yaml_2.2.1        xfun_0.24        
    ## [17] httr_1.4.2        dplyr_1.0.8       xml2_1.3.2        knitr_1.33       
    ## [21] generics_0.1.2    vctrs_0.3.8       systemfonts_1.0.2 webshot_0.5.2    
    ## [25] tidyselect_1.1.2  svglite_2.1.0     glue_1.4.2        R6_2.5.1         
    ## [29] fansi_1.0.2       rmarkdown_2.9     purrr_0.3.4       magrittr_2.0.1   
    ## [33] scales_1.1.1      htmltools_0.5.1.1 ellipsis_0.3.2    assertthat_0.2.1 
    ## [37] rvest_1.0.0       colorspace_2.0-1  utf8_1.2.2        stringi_1.6.2    
    ## [41] munsell_0.5.0     crayon_1.5.0
